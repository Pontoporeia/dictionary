
from nltk.corpus import wordnet

fList = open("frequencyList.txt","r")#Read the file
lines = fList.readlines()

eWords = open("eng_words_only.txt", "a")#Open file for writing

for w in lines:
    if not wordnet.synsets(w):#Comparing if word is non-English
        print 'not '+w
    else:#If word is an English word
        print 'yes '+w
        eWords.write(w)#Write to file 
        
eWords.close()#Close the file
