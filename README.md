# dictionary

#### Consigne dictionnaire dataset:

#### A garder:

- nom commun très connue
- mots commun
- nom de ville en dehors de le pays

#### A virer:

- mots peu utilisé / peu commun
- petit mot
- abréviation
- noms chimiques ou extrèmement technique
- trop scientifique technique trop obscure
- pas de nom composé
- 

note: on garde coprolite et coprophage

---

### différentes sources de data:

##### german:

https://github.com/gambolputty/dewiki-wordrank

http://gwicks.net/dictionaries.htm

http://corpus.leeds.ac.uk/frqc/internet-de-forms.num

https://www.ids-mannheim.de/digspra/kl/projekte/methoden/derewo

http://archive.ics.uci.edu/ml/datasets/Statlog+%28German+Credit+Data%29

https://sourceforge.net/projects/germandict/files/

https://opendata.stackexchange.com/questions/1390/german-english-dictionary

https://sourceforge.net/projects/germandict/

https://german.stackexchange.com/questions/491/where-can-i-find-a-parsable-list-of-german-words

https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists#Germanhttps://www-user.tu-chemnitz.de/~fri/ding/

https://www-user.tu-chemnitz.de/~fri/ding/

https://github.com/clld/wold2/blob/master/data/data.zip

##### russian:

https://github.com/Badestrand/russian-dictionary

https://en.openrussian.org/dictionary

https://github.com/hingston/russian

https://archive.ics.uci.edu/ml/datasets/Russian+Corpus+of+Biographical+Texts

https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists#Russian

https://github.com/sshra/database-russian-morphology

---

### Méthode:

pour le russe, voir pour les accents... et les lettres avec accents

traduction ?

pour l'allemand, chercher les mots composé pour peut-être éliminé des mots trop spécifiques ? --> regex ?

check nom propre et noms de ville

nouvelle méthode:
comparer multiple source de mots pour en sortir ceux qu'on retrouve le plus ?

### German

- ß = ss *deutch // oder sss
- ä =ae
- ü = ue
- ö=oe

### Russian

russian alphabet:

33 lettres

|   А   |   Б   |   В   |   Г   |   Д   |   Е   |   Ё   |   Ж   |   З   |   И   |   Й   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  /a/  |  /b/  |  /v/  |  /ɡ/  |  /d/  | /je/  | /jo/  |  /ʐ/  |  /z/  |  /i/  |  /j/  |
| **К** | **Л** | **М** | **Н** | **О** | **П** | **Р** | **С** | **Т** | **У** | **Ф** |
|  /k/  |  /l/  |  /m/  |  /n/  |  /o/  |  /p/  |  /r/  |  /s/  |  /t/  |  /u/  |  /f/  |
| **Х** | **Ц** | **Ч** | **Ш** | **Щ** | **Ъ** | **Ы** | **Ь** | **Э** | **Ю** | **Я** |
|  /x/  | /ts/  | /tɕ/  |  /ʂ/  | /ɕː/  |  /-/  |  /ɨ/  |  /ʲ/  |  /e/  | /ju/  | /ja/  |



### formatage:

minuscule

1 mot par ligne

pas d'espace blanc

pas d'accent

#### REGEX


regex special char: ```[.,\/#!$%\^&\*;:{}=\`-_~()]```


similar string:  `\b(\w+)\s+\1\b`

single line: `^\h*\R`
tabs: `\t`

single letter: `(^| ).( |$)`

ponctuation: `[^\w\s]`

numbers = `\d+[0-9]`

accents: `[æàáâãäåçèéêëìíîïœñòóôõöùúûüýÿ]`

line with a dot : `^.\..*$`

abréviation: ```^[^\s]{4}$```

`[àáâãäå]` = a
`æ` = ae
`ç` = c
`[èéêë]` = e 
`[ìíîï]` = i
`ñ` = n
`[òóôõö]` = o
`œ` = oe
`[ùúûü]` = u
`[ýÿ]` = y

`/^(?!.*АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ).*/`

2 listes:

tri alphabétique

tri par nombre de charactère 

Voir [organize_len.py](organize_len.py)
