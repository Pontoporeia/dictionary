
WOLD data dump
==============

Data of WOLD is published under the following license:
http://creativecommons.org/licenses/by/3.0/de/

It should be cited as

Haspelmath, Martin & Tadmor, Uri (eds.) 2009.
World Loanword Database.
Leipzig: Max Planck Institute for Evolutionary Anthropology.
(Available online at http://wold.clld.org, Accessed on 2015-07-21.)


This package contains files in csv format [1] with corresponding schema
descriptions according to [2], representing rows in database tables of
the WOLD web application [3,4].

[1] http://www.w3.org/TR/tabular-data-model/#syntax
[2] http://www.w3.org/TR/tabular-metadata/
[3] http://wold.clld.org
[4] https://github.com/clld/wold
