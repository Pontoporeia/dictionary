import os
import glob
import argparse
import csv

parser=argparse.ArgumentParser()
parser.add_argument('-l', '--language',
					required=True,
					dest='language',
                    help='language to set correct directory',
                    default=str
                    )
args = parser.parse_args()

lang = args.language
print("Current working directory : "+os.getcwd())

if lang == "german":
    mod = "/german/lists/"
    print("german language selected")
    path = os.path.join(os.getcwd()+mod)
    file = glob.glob(path+"list*")
    file.sort()
    x=0
    dctnry = {}    
    for f in file:
        with open(f,'r', encoding = "ISO-8859-1") as lists:
            dctnry['l'+str(x)]=[]
            lines = lists .readlines()
            for i in lines:
                dctnry['l'+str(x)].append(i.replace("\n",""))
        x=x+1
    print(dctnry)
elif lang == "russian":
    mod = "/russian/lists/"
    print("russian language selected")
    path = os.path.join(os.getcwd()+mod)
    file = glob.glob(path+"list*")
    file.sort()
    print(file)
    x=0
    dctnry = {}    
    for f in file:
        with open(f,'r', encoding = "UTF-8") as lists:
            dctnry['l'+str(x)]=[]
            lines = lists .readlines()
            for i in lines:
                dctnry['l'+str(x)].append(i.replace("\n",""))
        x=x+1
    # print(dictry)
    print("Number of lists in dctnry is "+str(x))
else:
    print("Selected language is not supported. Pls correct it")
    exit()
